@extends('layout.app')
@section('page_title')
    Car Rental Portal | Page details
@endsection
@section('content')

@if($page)
<section class="page-header aboutus_page">
  <div class="container">
    <div class="page-header_wrap">
      <div class="page-heading">
        <h1><?php   echo htmlentities($page->PageName); ?></h1>
      </div>
      <ul class="coustom-breadcrumb">
        <li><a href="#">Home</a></li>
        <li><?php   echo htmlentities($page->PageName); ?></li>
      </ul>
    </div>
  </div>
  <!-- Dark Overlay-->
  <div class="dark-overlay"></div>
</section>
<section class="about_us section-padding">
  <div class="container">
    <div class="section-header text-center">


      <h2><?php   echo htmlentities($page->PageName); ?></h2>
      <p><?php  echo $page->detail; ?> </p>
    </div>
@endif
  </div>
</section>
<!-- /About-us--> 


@endsection	