@extends('User/user_app')

@section('page_title')
Car Rental Portal | My Profile
@endsection


@section('title-bar')
<div class="page-heading">
    <h1>Post Testimonial</h1>
</div>
<ul class="coustom-breadcrumb">
    <li><a href="#">Home</a></li>
    <li>Post Testimonial</li>
</ul>
@endsection


@section('userinfo')

<h5 class="uppercase underline">Post a Testimonial</h5>
@if(session('status'))
	@if(session('status')=='error')
		<div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
	@else
		<div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
	@endif
@endif
<form method="post" action="{{route('post_test')}}">
	@csrf
    <div class="form-group">
        <label class="control-label">Testimonail</label>
        <textarea class="form-control white_bg" name="testimonial" rows="4" required=""></textarea>
    </div>
    <div class="form-group">
        <button type="submit" class="btn">Save 
        	<span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
        </button>
    </div>
</form>
@endsection