@extends('User/user_app')

@section('page_title')
Car Rental Portal | My Profile
@endsection


@section('title-bar')
<div class="page-heading">
    <h1>My Booking</h1>
</div>
<ul class="coustom-breadcrumb">
    <li><a href="#">Home</a></li>
    <li>My Booking</li>
</ul>
@endsection


@section('userinfo')

<h5 class="uppercase underline">My Booking/s </h5>
<div class="my_vehicles_list">
    <ul class="vehicle_listing">
	@if(count($bookings) > 0)
	@foreach($bookings as $key => $result)
        <li>
            <div class="vehicle_img"> 
                <a href="{{route('vehical_details',$result->vehicle->id)}}">
            	<img src="
                    admin/img/vehicleimages/<?php echo htmlentities($result->vehicle->Vimage1);?>" alt="image">
                </a> 
            </div>
            <div class="vehicle_title">
                <h6>
                	<a href="{{route('vehical_details',$result->id)}}">
                	
                		<?php echo htmlentities($result->BrandName);?> , <?php echo htmlentities($result->vehicle->VehiclesTitle);?>
                			
            		</a>
            	</h6>
                <p><b>From Date:</b> <?php echo htmlentities($result->FromDate);?><br /> <b>To Date:</b> <?php echo htmlentities($result->ToDate);?></p>
                </div>
            <?php if($result->Status==1){ ?>
            <div class="vehicle_status"> <a href="#" class="btn-primary btn-block text-center">Confirmed</a>
                    <div class="clearfix"></div>
            </div>

            <?php } else if($result->Status==2) { ?>
            <div class="vehicle_status"> <a href="#" class="btn-danger btn-block text-center">Cancelled</a>
                <div class="clearfix"></div>
            </div>

            <?php } else if($result->Status==3) { ?>
            <div class="vehicle_status"> <a href="#" class="btn-success btn-block text-center">Completed</a>
                <div class="clearfix"></div>
            </div>

            <?php } else { ?>
            <div class="vehicle_status"> <a href="#" class="btn-warning btn-block text-center">Not Confirmed yet</a>
                <div class="clearfix"></div>
            </div>

            <?php } ?>
            <div style="float: left">
                <br>
                <p><b>Message:</b> <?php echo htmlentities($result->message);?> </p>
            </div>
        </li>
        @endforeach
        @endif


    </ul>
</div>
@endsection