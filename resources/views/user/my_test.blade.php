@extends('User/user_app')

@section('page_title')
Car Rental Portal | My Profile
@endsection


@section('title-bar')
<div class="page-heading">
    <h1>My Testimonials</h1>
  </div>
  <ul class="coustom-breadcrumb">
    <li><a href="#">Home</a></li>
    <li>My Testimonials</li>
  </ul>
@endsection


@section('userinfo')
  <script type="text/javascript">
  	$(function(){
  		$("#user_section").removeClass('col-md-6');
  		$("#user_section").addClass('col-md-8');
  	})
  </script>

<h5 class="uppercase underline">My Testimonials </h5>
  <div class="my_vehicles_list">
    <ul class="vehicle_listing">
	@if(count($testimonials)>0)
	@foreach($testimonials as $result)
      <li>
   
        <div>
         <p><?php echo htmlentities($result->Testimonial);?> </p>
           <p><b>Posting Date:</b><?php echo htmlentities($result->created_at);?> </p>
        </div>
        <?php if($result->status==1){ ?>
         <div class="vehicle_status"> <a class="btn outline btn-xs active-btn">Active</a>

          <div class="clearfix"></div>
          </div>
          <?php } else {?>
       <div class="vehicle_status"> <a href="#" class="btn outline btn-xs">Waiting for approval</a>
          <div class="clearfix"></div>
          </div>
          <?php } ?>
      </li>
    @endforeach
    @endif
      
    </ul>
   
  </div>


@endsection