@extends('layout.app')

@section('content')

 <style>
    .errorWrap {
	    padding: 10px;
	    margin: 0 0 20px 0;
	    background: #fff;
	    border-left: 4px solid #dd3d36;
	    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	}
	.succWrap{
	    padding: 10px;
	    margin: 0 0 20px 0;
	    background: #fff;
	    border-left: 4px solid #5cb85c;
	    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	}
</style>

<!--Page Header-->
<section class="page-header profile_page">
  <div class="container">
    <div class="page-header_wrap">
    	@yield('title-bar')
    </div>
  </div>
  <!-- Dark Overlay-->
  <div class="dark-overlay"></div>
</section>
<!-- /Page Header--> 
<section class="user_profile inner_pages">
  <div class="container">
    <div class="user_profile_info gray-bg padding_4x4_40">
      <div class="upload_user_logo"> <img src="assets/images/dealer-logo.jpg" alt="image">
      </div>

      <div class="dealer_info">
        <h5>{{Auth::user()->username}}</h5>
        <p>{{Auth::user()->address}}<br>
         {{Auth::user()->city}}&nbsp;{{Auth::user()->country}}</p>
      </div>
    </div>
  
    <div class="row">
      <div class="col-md-3 col-sm-3">
        @include('layout.components.sidebar')
      <div class="col-md-6 col-sm-8" id="user_section">
        <div class="profile_wrap">
          @yield('userinfo')
        </div>
      </div>
    </div>
  </div>
</section>
<!--/Profile-setting--> 


@endsection