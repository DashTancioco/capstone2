@extends('User/user_app')

@section('page_title')
Car Rental Portal | My Profile
@endsection


@section('title-bar')
<div class="page-heading">
    <h1>Update Password</h1>
  </div>
  <ul class="coustom-breadcrumb">
    <li><a href="#">Home</a></li>
    <li>Update Password</li>
  </ul>
@endsection


@section('userinfo')
<form id="chngpwd" method="post" action="{{route('pass_update')}}">
    @csrf    
    <div class="gray-bg field-title">
      <h6>Update password</h6>
    </div>
     	@if(session('status'))
     		@if(session('status')=='error')
     			<div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
     		@else
     			<div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
     		@endif
     	@endif
    <div class="form-group">
      <label class="control-label">Current Password</label>
      <input class="form-control white_bg" id="password" name="password"  type="password" required>
    </div>
    <div class="form-group">
      <label class="control-label">Password</label>
      <input class="form-control white_bg" id="newpassword" type="password" name="newpassword"  minlength="6" required validate>
    </div>
    <div class="form-group">
      <label class="control-label">Confirm Password</label>
      <input class="form-control white_bg" id="confirmpassword" type="password" name="confirmpassword" minlength="6" required>
    </div>
  
    <div class="form-group">
       <input type="submit" value="Update" name="update" id="update_pass_btn" class="btn btn-block">
    </div>
  </form>

  <script type="text/javascript">
  	$(function(){
  		$("#update_pass_btn").click(function(e){
	  		e.preventDefault();
	  		new_pass=$("#newpassword").val();
	  		confirm_pass=$("#confirmpassword").val();
	  		if(new_pass.length <6 ){
	  			alert("Check password length!");
	  			return false;
	  		}
	  		if(new_pass!=confirm_pass){
	  			alert("Password Confirm!");
	  			return false;
	  		}
	  		
	  		$("#chngpwd").submit();
	  	});
  	})
  	

  </script>

@endsection