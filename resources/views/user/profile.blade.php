@extends('User/user_app')

@section('page_title')
Car Rental Portal | My Profile
@endsection


@section('title-bar')
<div class="page-heading">
	<h1>Your Profile</h1>
</div>
<ul class="coustom-breadcrumb">
	<li><a href="#">Home</a></li>
	<li>Profile</li>
</ul>
@endsection


@section('userinfo')


<h5 class="uppercase underline">Genral Settings</h5>
 @if (session('success'))
 <div class="succWrap"><strong>SUCCESS</strong>:{{session('success')}}</div>
 @endif
  <form  method="post" action="{{route('profile_update',Auth::user()->id )}}">
  	@csrf
  	@method('put')
   <div class="form-group">
      <label class="control-label">Reg Date -</label>
        {{Auth::user()->created_at}}
    </div>
 	@if(Auth::user()->updated_at!="")
    <div class="form-group">
      <label class="control-label">Last Update at  -</label>
      {{Auth::user()->updated_at}}
    </div>
    @endif
    <div class="form-group">
      <label class="control-label">Full Name</label>
      <input class="form-control white_bg" name="username" value="{{Auth::user()->username}}" id="fullname" type="text"  required>
    </div>
    <div class="form-group">
      <label class="control-label">Email Address</label>
      <input class="form-control white_bg" value="{{Auth::user()->email}}" name="email" id="email" type="email" required readonly>
    </div>
    <div class="form-group">
      <label class="control-label">Phone Number</label>
      <input class="form-control white_bg" name="contact_no" value="{{Auth::user()->contact_no}}" id="phone-number" type="text" required>
    </div>
    <div class="form-group">
      <label class="control-label">Date of Birth&nbsp;(dd/mm/yyyy)</label>
      <input class="form-control white_bg" value="{{Auth::user()->dob}}" name="dob" placeholder="dd/mm/yyyy" id="birth-date" type="text" >
    </div>
    <div class="form-group">
      <label class="control-label">Your Address</label>
      <textarea class="form-control white_bg" name="address" rows="4" >{{Auth::user()->address}}</textarea>
    </div>
    <div class="form-group">
      <label class="control-label">Country</label>
      <input class="form-control white_bg"  id="country" name="country" value="{{Auth::user()->country}}" type="text">
    </div>
    <div class="form-group">
      <label class="control-label">City</label>
      <input class="form-control white_bg" id="city" name="city" value="{{Auth::user()->city}}" type="text">
    </div>
   
    <div class="form-group">
      <button type="submit" class="btn">Save Changes <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
    </div>
  </form>

@endsection