<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>@yield('page_title', 'Car Rental Portal')</title>
    <!--Bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/owl.transitions.css')}}" type="text/css">
    <link href="{{asset('assets/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap-slider.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" id="switcher-css" type="text/css" href="{{asset('assets/switcher/css/switcher.css')}}" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('assets/switcher/css/red.css')}}" title="red" media="all"
        data-default-color="true" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('assets/switcher/css/orange.css')}}" title="orange" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('assets/switcher/css/blue.css')}}" title="blue" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('assets/switcher/css/pink.css')}}" title="pink" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('assets/switcher/css/green.css')}}" title="green" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('assets/switcher/css/purple.css')}}" title="purple" media="all" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="{{asset('assets/images/favicon-icon/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="{{asset('assets/images/favicon-icon/apple-touch-icon-114-precomposed.html')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
        href="{{asset('assets/images/favicon-icon/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('assets/images/favicon-icon/apple-touch-icon-57-precomposed.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon-icon/favicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    
</head>

<body>

    <!-- Start Switcher -->
    @include('layout.components.colorswitcher')
    <!-- /Switcher -->

    <!--Header-->
    @include('layout.components.header')
    <!-- /Header -->

    @yield('content')


    <!--Footer -->
    @include('layout.components.footer')
    <!-- /Footer-->

    <!--Back to top-->
    <div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>
    <!--/Back to top-->

    <!--Login-Form -->
    @include('layout.components.forms.login')
    <!--/Login-Form -->

    <!--Register-Form -->
    @include('layout.components.forms.registration')
    <!--/Register-Form -->

    <!--Forgot-password-Form -->
    @include('layout.components.forms.forgotpassword')
    <!--/Forgot-password-Form -->

    <!-- Scripts -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/interface.js')}}"></script>
    <!--Switcher-->
    <script src="{{asset('assets/switcher/js/switcher.js')}}"></script>
    <!--bootstrap-slider-JS-->
    <script src="{{asset('assets/js/bootstrap-slider.min.js')}}"></script>
    <!--Slider-JS-->
    <script src="{{asset('assets/js/slick.min.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>

</body>

<!-- Mirrored from themes.webmasterdriver.net/carforyou/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Jun 2017 07:22:11 GMT -->

</html>