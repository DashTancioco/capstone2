<div class="profile_nav">
    <ul>
        <li><a href="{{route('profile')}}">Profile Settings</a></li>
        <li><a href="{{route('update_password')}}">Update Password</a></li>
        <li><a href="{{route('my_booking')}}">My Booking</a></li>
        <li><a href="{{route('post_testimonial')}}">Post a Testimonial</a></li>
        <li><a href="{{route('my_testimonials')}}">My Testimonial</a></li>
        <li><a href="{{route('logout')}}"  
        	onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Sign Out
            </a>
        </li>
    </ul>
</div>
</div>