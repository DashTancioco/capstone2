<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <h6>About Us</h6>
                    <ul>
                    	<li><a href="{{route('page', 'aboutus')}}">About Us</a></li>
                        <li><a href="{{route('page', 'faqs')}}">FAQs</a></li>
                        <li><a href="{{route('page', 'privacy')}}">Privacy</a></li>
                        <li><a href="{{route('page', 'terms')}}">Terms of use</a></li>
                        <li><a href="{{route('admin')}}">Admin Login</a></li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-6">
                    <h6>Subscribe Newsletter</h6>
                    <div class="newsletter-form">
                        <form>
                            <div class="form-group">
                                <input type="email" id="subscriberemail" class="form-control newsletter-input"
                                    required placeholder="Enter Email Address" />
                            </div>
                            <button type="button" id="emailsubscibe" class="btn btn-block">Subscribe <span
                                    class="angle_arrow"><i class="fa fa-angle-right"
                                        aria-hidden="true"></i></span></button>
                        </form>
                        <p class="subscribed-text">*We send great deals and latest auto news to our subscribed users
                            very week.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6 text-right">
                    <div class="footer_widget">
                        <p>Connect with Us:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <p class="copy-right">Copyright &copy; 2017 Car Rental Portal. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    $("#emailsubscibe").click(function(){
        email=$("#subscriberemail").val();
        $.ajax({
            url:"{{route('emailsubscibe')}}",
            type:'post',
            data:{
                _token:"{{csrf_token()}}",
                email:email,
            },
            success:function(result){
                if(result==1){
                    alert('Subscribed successfully.');
                    $("#subscriberemail").val('');
                }else{
                    alert('Already Subscribed.');
                    $("#subscriberemail").val('');
                }
            },
            error:function(e){
                alert('Something went wrong. Please try again');
            }
        })
    })
</script>