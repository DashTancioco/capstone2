<header>
    <div class="default-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <div class="logo"> <a href="{{route('home')}}"><img src="{{asset('assets/images/logo.png')}}" alt="image" /></a> </div>
                </div>
                <div class="col-sm-9 col-md-10">
                    <div class="header_info">
                        <div class="header_widgets">
                            <div class="circle_icon"> <i class="fa fa-envelope" aria-hidden="true"></i> </div>
                            <p class="uppercase_text">For Support Mail us : </p>
                            <a href="mailto:info@example.com">info@example.com</a>
                        </div>
                        <div class="header_widgets">
                            <div class="circle_icon"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
                            <p class="uppercase_text">Service Helpline Call Us: </p>
                            <a href="tel:61-1234-5678-09">+91-1234-5678-9</a>
                        </div>
                        <div class="social-follow">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        @guest
                        <div class="login_btn"> <a href="#loginform" class="btn btn-xs uppercase" data-toggle="modal"
                                data-dismiss="modal">
                                Login / Register</a> </div>
                        @else
                            Welcome To Car rental portal
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Navigation -->
    <nav id="navigation_bar" class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button id="menu_slide" data-target="#navigation" aria-expanded="false" data-toggle="collapse"
                    class="navbar-toggle collapsed" type="button"> <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="header_wrap">
                <div class="user_login">
                    <ul>
                        <li class="dropdown"> <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle" aria-hidden="true"></i>
                            @auth
                            {{ Auth::user()->username }}
                            @endauth
                            <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu">
                                @auth
                                <li><a href="{{route('profile')}}">Profile Settings</a></li>
                                <li><a href="{{route('update_password')}}">Update Password</a></li>
                                <li><a href="{{route('my_booking')}}">My Booking</a></li>
                                <li><a href="{{route('post_testimonial')}}">Post a Testimonial</a></li>
                                <li><a href="{{route('my_testimonials')}}">My Testimonial</a></li>
                                <li><a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
		                                Sign Out
		                            </a>
		                        </li>
		                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                              
                                @else
                                <li><a href="#loginform" data-toggle="modal" data-dismiss="modal">Profile Settings</a>
                                </li>
                                <li><a href="#loginform" data-toggle="modal" data-dismiss="modal">Update Password</a>
                                </li>
                                <li><a href="#loginform" data-toggle="modal" data-dismiss="modal">My Booking</a></li>
                                <li><a href="#loginform" data-toggle="modal" data-dismiss="modal">Post a Testimonial</a>
                                </li>
                                <li><a href="#loginform" data-toggle="modal" data-dismiss="modal">My Testimonial</a>
                                </li>
                                <li><a href="#loginform" data-toggle="modal" data-dismiss="modal">Sign Out</a></li>
                                @endauth
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="header_search">
                    <div id="search_toggle"><i class="fa fa-search" aria-hidden="true"></i></div>
                    <form action="#" method="get" id="header-search-form">
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li><a href="{{route('page','aboutus')}}">About Us</a></li>
                    <li><a href="{{route('car_listing')}}">Car Listing</a>
                    <li><a href="{{route('page','faqs')}}">FAQs</a></li>
                    <li><a href="{{route('contact_us')}}">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navigation end -->

</header>