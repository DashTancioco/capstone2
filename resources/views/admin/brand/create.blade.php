@extends('admin.components.layout')

@section('title')
Car Rental Portal | Admin Create Brand
@endsection


@section('content')

<h2 class="page-title">Create Brand</h2>

	<div class="row">
		<div class="col-md-10">
			<div class="panel panel-default">
				<div class="panel-heading">Form fields</div>
				<div class="panel-body">
					<form method="post"  class="form-horizontal" action="{{route('admin.add_brand')}}">
					@csrf

                 	@if(session('status'))
			     		@if(session('status')=='error')
			     			<div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
			     		@else
			     			<div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
			     		@endif
			     	@endif

						<div class="form-group">
							<label class="col-sm-4 control-label">Brand Name</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="brand" id="brand" required>
							</div>
						</div>
						<div class="hr-dashed"></div>
						
					
			
						
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
			
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>
						</div>

					</form>

				</div>
			</div>
		</div>
		
	</div>
@endsection