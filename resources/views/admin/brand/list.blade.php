@extends('admin.components.layout')

@section('title')
Car Rental Portal | Admin Create Brand
@endsection


@section('content')

<h2 class="page-title">Manage Brands</h2>
	<!-- Zero Configuration Table -->
	<div class="panel panel-default">
		<div class="panel-heading">Listed  Brands</div>
		<div class="panel-body">
		@if(session('status'))
     		@if(session('status')=='error')
     			<div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
     		@else
     			<div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
     		@endif
     	@endif
			<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
				<thead>
					<tr>
					<th>#</th>
						<th>Brand Name</th>
						<th>Creation Date</th>
						<th>Updation date</th>
					
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
					<th>#</th>
						<th>Brand Name</th>
						<th>Creation Date</th>
						<th>Updation date</th>
					
						<th>Action</th>
					</tr>
					</tr>
				</tfoot>
				<tbody>


				@foreach($brands as $key => $result)
					<tr>
						<td>{{$key+1}}</td>
						<td><?php echo htmlentities($result->BrandName);?></td>
						<td><?php echo htmlentities($result->created_at);?></td>
						<td><?php echo htmlentities($result->updated_at);?></td>
						<td>
							<a href="{{route('admin.brand.edit', $result->id)}}">
								<i class="fa fa-edit"></i></a>&nbsp;&nbsp;
							<a href="{{ route('admin.brand.destroy', $result->id) }}" onclick="return confirm('Do you want to delete');">
								<i class="fa fa-close"></i></a></td>
					</tr>
				@endforeach					
				</tbody>
			</table>

	

		</div>
	</div>
@endsection