@extends('admin.components.layout')

@section('content')
<script type="text/javascript">
    function valid() {
        if (document.chngpwd.newpassword.value != document.chngpwd.confirmpassword.value) {
            alert("New Password and Confirm Password Field do not match  !!");
            document.chngpwd.confirmpassword.focus();
            return false;
        }
        return true;
    }
</script>

<h2 class="page-title">Change Password</h2>
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form fields</div>
                <div class="panel-body">
                    <form method="post" class="form-horizontal" action="{{route('admin.update_pass')}}" id="uppass-form">
                        @csrf
                     	@if(session('status'))
				     		@if(session('status')=='error')
				     			<div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
				     		@else
				     			<div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
				     		@endif
				     	@endif
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Current Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="password"
                                    id="password" required>
                            </div>
                        </div>
                        <div class="hr-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">New Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="newpassword"
                                    id="newpassword" required>
                            </div>
                        </div>
                        <div class="hr-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Confirm Password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="confirmpassword"
                                    id="confirmpassword" required>
                            </div>
                        </div>
                        <div class="hr-dashed"></div>



                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">

                                <button class="btn btn-primary" id="up_pass_btn" type="submit">Save
                                    changes</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
    	$("#up_pass_btn").click(function(){
    		newpassword=$("#newpassword").val();
    		confirmpassword=$("#confirmpassword").val();
    		if(newpassword.length <6){
    			alert('Please check password length!');
    			return false;
    		}

    		if(newpassword != confirmpassword){
    			alert('Please check password confirm!');
    			return false;
    		}
    		$("#uppass-form").submit();

    	})
    </script>

@endsection