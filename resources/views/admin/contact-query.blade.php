@extends('admin.components.layout')
@section('title')
Car Rental Portal | Admin Dashboard
@endsection
@section('content')

<h2 class="page-title">Manage Contact Us Queries</h2>

<!-- Zero Configuration Table -->
<div class="panel panel-default">
    <div class="panel-heading">User queries</div>
    <div class="panel-body">
        @if(session('status'))
        @if(session('status')=='error')
        <div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
        @else
        <div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
        @endif
        @endif
        <table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact No</th>
                    <th>Message</th>
                    <th>Posting date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact No</th>
                    <th>Message</th>
                    <th>Posting date</th>
                    <th>Action</th>
                </tr>
                </tr>
            </tfoot>
            <tbody>
				@foreach($queries as $key => $result)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><?php echo htmlentities($result->name);?></td>
                    <td><?php echo htmlentities($result->EmailId);?></td>
                    <td><?php echo htmlentities($result->ContactNumber);?></td>
                    <td><?php echo htmlentities($result->Message);?></td>
                    <td><?php echo htmlentities($result->created_at);?></td>
                    <?php if($result->status==1)
					{
						?><td>Read</td>
                    <?php } else {?>

                    <td><a href="{{route('admin.status_read', $result->id)}}"
                            onclick="return confirm('Do you really want to read')">Pending</a>
                    </td>
                    <?php } ?>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>

@endsection