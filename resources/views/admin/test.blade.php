@extends('admin.components.layout')
@section('title')
Car Rental Portal | Admin Dashboard
@endsection
@section('content')

<h2 class="page-title">Manage Testimonials</h2>

<!-- Zero Configuration Table -->
<div class="panel panel-default">
    <div class="panel-heading">User Testimonials</div>
    <div class="panel-body">
        @if(session('status'))
            @if(session('status')=='error')
                <div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
            @else
                <div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
            @endif
        @endif
        <table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Testimonials</th>
                    <th>Posting date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Testimonials</th>
                    <th>Posting date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>

                @foreach($tests as $key=>$result)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><?php echo htmlentities($result->user->username);?></td>
                    <td><?php echo htmlentities($result->user->email);?></td>
                    <td><?php echo htmlentities($result->Testimonial);?></td>
                    <td><?php echo htmlentities($result->created_at);?></td>
                    <td>
                        @if($result->status=="" || $result->status==0)
                         Inactive
                        @else
                        Active
                        @endif
                    </td>
                    <td>
                        @if($result->status=="" || $result->status==0)
                        <a href="{{route('admin.test_change', ['id'=>$result->id, 'status'=>1] )}}"
                            onclick="return confirm('Do you really want to Active')"> Active</a>
                        @else
                        <a href="{{route('admin.test_change', ['id'=>$result->id, 'status'=>0] )}}"
                            onclick="return confirm('Do you really want to Active')"> Inactive</a>
                        @endif
                    </td>
                 
                </tr>
                @endforeach

            </tbody>
        </table>



    </div>
</div>

@endsection