@extends('admin.components.layout')
@section('title')
Car Rental Portal | Admin Dashboard
@endsection
@section('content')

<h2 class="page-title">Dashboard</h2>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body bk-primary text-light">
                        <div class="stat-panel text-center">
                            <div class="stat-panel-number h1 ">{{$user_count}}</div>
                            <div class="stat-panel-title text-uppercase">Reg Users</div>
                        </div>
                    </div>
                    <a href="{{route('admin.reg_users')}}" class="block-anchor panel-footer">Full Detail <i
                            class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body bk-success text-light">
                        <div class="stat-panel text-center">

                            <div class="stat-panel-number h1 ">{{$vehicle_count}}</div>
                            <div class="stat-panel-title text-uppercase">Listed Vehicles</div>
                        </div>
                    </div>
                    <a href="{{route('admin.manage_vehicles')}}" class="block-anchor panel-footer text-center">Full Detail &nbsp; <i
                            class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body bk-info text-light">
                        <div class="stat-panel text-center">


                            <div class="stat-panel-number h1 ">{{$booking_count}}</div>
                            <div class="stat-panel-title text-uppercase">Total Bookings</div>
                        </div>
                    </div>
                    <a href="{{route('admin.manage_bookings')}}" class="block-anchor panel-footer text-center">Full Detail &nbsp; <i
                            class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body bk-warning text-light">
                        <div class="stat-panel text-center">

                            <div class="stat-panel-number h1 ">{{$brand_count}}</div>
                            <div class="stat-panel-title text-uppercase">Listed Brands</div>
                        </div>
                    </div>
                    <a href="{{route('admin.manage_brands')}}" class="block-anchor panel-footer text-center">Full Detail &nbsp; <i
                            class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
<div class="row">
    <div class="col-md-12">


        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-body bk-primary text-light">
                                <div class="stat-panel text-center">

                                    <div class="stat-panel-number h1 ">{{$scribe_count}}</div>
                                    <div class="stat-panel-title text-uppercase">Subscibers</div>
                                </div>
                            </div>
                            <a href="{{route('admin.manage_subscribers')}}" class="block-anchor panel-footer">Full Detail <i
                                    class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-body bk-success text-light">
                                <div class="stat-panel text-center">

                                    <div class="stat-panel-number h1 ">{{$query_count}}</div>
                                    <div class="stat-panel-title text-uppercase">Queries</div>
                                </div>
                            </div>
                            <a href="{{route('admin.manage_conactusquery')}}" class="block-anchor panel-footer text-center">Full Detail
                                &nbsp; <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-body bk-info text-light">
                                <div class="stat-panel text-center">



                                    <div class="stat-panel-number h1 ">{{$test_count}}</div>
                                    <div class="stat-panel-title text-uppercase">Testimonials</div>
                                </div>
                            </div>
                            <a href="{{route('admin.testimonials')}}" class="block-anchor panel-footer text-center">Full Detail &nbsp;
                                <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @endsection