<nav class="ts-sidebar">
    <ul class="ts-sidebar-menu">

        <li class="ts-label">Main</li>
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>

        <li><a href="#"><i class="fa fa-files-o"></i> Brands</a>
            <ul>
                <li><a href="{{route('admin.create_brand')}}">Create Brand</a></li>
                <li><a href="{{route('admin.manage_brands')}}">Manage Brands</a></li>
            </ul>
        </li>

        <li><a href="#"><i class="fa fa-sitemap"></i> Vehicles</a>
            <ul>
                <li><a href="{{route('admin.post_vehical')}}">Post a Vehicle</a></li>
                <li><a href="{{route('admin.manage_vehicles')}}">Manage Vehicles</a></li>
            </ul>
        </li>
        <li><a href="{{route('admin.manage_bookings')}}"><i class="fa fa-users"></i> Manage Booking</a></li>

        <li><a href="{{route('admin.testimonials')}}"><i class="fa fa-table"></i> Manage Testimonials</a></li>
        <li><a href="{{route('admin.manage_conactusquery')}}"><i class="fa fa-desktop"></i> Manage Conatctus Query</a></li>
		<li><a href="{{route('admin.reg_users')}}"><i class="fa fa-users"></i> Reg Users</a></li>
        <li><a href="{{route('admin.manage_pages')}}"><i class="fa fa-files-o"></i> Manage Pages</a></li>
        <li><a href="{{route('admin.update_contactinfo')}}"><i class="fa fa-files-o"></i> Update Contact Info</a></li>

        <li><a href="{{route('admin.manage_subscribers')}}"><i class="fa fa-table"></i> Manage Subscribers</a></li>

    </ul>
</nav>