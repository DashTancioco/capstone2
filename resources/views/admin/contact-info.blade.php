@extends('admin.components.layout')
@section('title')
Car Rental Portal | Admin Dashboard
@endsection
@section('content')

<h2 class="page-title">Update Contact Info</h2>

<div class="row">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">Form fields</div>
            <div class="panel-body">
                <form method="post"  class="form-horizontal" action="{{route('admin.store_contact_info', $result->id)}}">
                	@csrf
                	@method('put')
                    @if(session('status'))
			     		@if(session('status')=='error')
			     			<div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
			     		@else
			     			<div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
			     		@endif
			     	@endif
                    <div class="form-group">
                        <label class="col-sm-4 control-label"> Address</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="address" id="address"
                                required><?php echo htmlentities($result->Address);?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"> Email id</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" name="email" id="email"
                                value="<?php echo htmlentities($result->EmailId);?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"> Contact Number </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control"
                                value="<?php echo htmlentities($result->ContactNo);?>" name="contactno" id="contactno"
                                required>
                        </div>
                    </div>
                    <div class="hr-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">

                            <button class="btn btn-primary" name="submit" type="submit">Update</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

</div>
@endsection