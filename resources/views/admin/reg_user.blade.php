@extends('admin.components.layout')
@section('title')
Car Rental Portal | Admin Dashboard
@endsection
@section('content')

<h2 class="page-title">Registered Users</h2>

<!-- Zero Configuration Table -->
<div class="panel panel-default">
    <div class="panel-heading">Reg Users</div>
    <div class="panel-body">
        @if(session('status'))
        @if(session('status')=='error')
        <div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
        @else
        <div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
        @endif
        @endif
        <table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th> Name</th>
                    <th>Email </th>
                    <th>Contact no</th>
                    <th>DOB</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Reg Date</th>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th> Name</th>
                    <th>Email </th>
                    <th>Contact no</th>
                    <th>DOB</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Reg Date</th>
                </tr>
                </tr>
            </tfoot>
            <tbody>
				@foreach($reg_users as $key => $result)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><?php echo htmlentities($result->username);?></td>
                    <td><?php echo htmlentities($result->email);?></td>
                    <td><?php echo htmlentities($result->contact_no);?></td>
                    <td><?php echo htmlentities($result->dob);?></td>
                    <td><?php echo htmlentities($result->address);?></td>
                    <td><?php echo htmlentities($result->city);?></td>
                    <td><?php echo htmlentities($result->country);?></td>
                    <td><?php echo htmlentities($result->created_at);?></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>

@endsection