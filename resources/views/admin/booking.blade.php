@extends('admin.components.layout')

@section('title')
Car Rental Portal | Admin Create Brand
@endsection


@section('content')

<h2 class="page-title">Manage Bookings</h2>

	<!-- Zero Configuration Table -->
	<div class="panel panel-default">
		<div class="panel-heading">Bookings Info</div>
		<div class="panel-body">
     	@if(session('status'))
     		@if(session('status')=='error')
     			<div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
     		@else
     			<div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
     		@endif
     	@endif
			<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
				<thead>
					<tr>
					<th>#</th>
						<th>Name</th>
						<th>Vehicle</th>
						<th>From Date</th>
						<th>To Date</th>
						<th>Message</th>
						<th>Status</th>
						<th>Posting date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
					<th>#</th>
					<th>Name</th>
						<th>Vehicle</th>
						<th>From Date</th>
						<th>To Date</th>
						<th>Message</th>
						<th>Status</th>
						<th>Posting date</th>
						<th>Action</th>
					</tr>
				</tfoot>
				<tbody>
					@foreach($bookings as $key=> $result)
					<tr>
						<td>{{$key+1}}</td>
						<td><?php echo htmlentities($result->user->username);?></td>
						<td><a href="{{route('admin.vehicle_edit', $result->VehicleId)}}">
							<?php echo htmlentities($result->vehicle->brand->BrandName);?> , <?php echo htmlentities($result->vehicle->VehiclesTitle);?></td>
						<td><?php echo htmlentities($result->FromDate);?></td>
						<td><?php echo htmlentities($result->ToDate);?></td>
						<td><?php echo htmlentities($result->message);?></td>
						<td>
							@if($result->Status == 0)
								Not Confirmed yet
							@elseif($result->Status ==1)
								Confirmed
							@elseif($result->Status ==2)
								Cancelled
							@else
								Completed	
							@endif
						</td>
						<td>{{$result->created_at}}</td>
						<td>
							<a href="{{route('manage-bookings-confirm', $result->id)}}" onclick="return confirm('Do you really want to Confirm this booking')"> Confirm</a> /
							<a href="{{route('manage-bookings-cancel', $result->id)}}" onclick="return confirm('Do you really want to Cancel this Booking')"> Cancel</a> /
							<a href="{{route('manage-bookings-complete', $result->id)}}" onclick="return confirm('Do you really want to Complete this Booking')"> Complete</a> 
						</td>

					</tr>
					@endforeach
				</tbody>
			</table>

	

		</div>
	</div>
@endsection