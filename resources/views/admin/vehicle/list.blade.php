@extends('admin.components.layout')

@section('title')
Car Rental Portal | Admin Create Brand
@endsection


@section('content')

<h2 class="page-title">Manage Vehicles</h2>

<!-- Zero Configuration Table -->
<div class="panel panel-default">
    <div class="panel-heading">Vehicle Details</div>
    <div class="panel-body">
        @if(session('status'))
        @if(session('status')=='error')
        <div class="errorWrap"><strong>ERROR</strong>:{{session('msg')}} </div>
        @else
        <div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
        @endif
        @endif
        <table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Vehicle Title</th>
                    <th>Brand </th>
                    <th>Price Per day</th>
                    <th>Fuel Type</th>
                    <th>Model Year</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Vehicle Title</th>
                    <th>Brand </th>
                    <th>Price Per day</th>
                    <th>Fuel Type</th>
                    <th>Model Year</th>
                    <th>Action</th>
                </tr>
                </tr>
            </tfoot>
            <tbody>
            @foreach($vehicles as $key => $result)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><?php echo htmlentities($result->VehiclesTitle);?></td>
                    <td><?php echo htmlentities($result->brand->BrandName);?></td>
                    <td><?php echo htmlentities($result->PricePerDay);?></td>
                    <td><?php echo htmlentities($result->FuelType);?></td>
                    <td><?php echo htmlentities($result->ModelYear);?></td>
                    <td><a href="{{route('admin.vehicle_edit', $result->id)}}"><i
                                class="fa fa-edit"></i></a>&nbsp;&nbsp;
                        <a href="{{route('admin.vehicle_delele', $result->id)}}"
                            onclick="return confirm('Do you want to delete');"><i class="fa fa-close"></i></a></td>
                </tr>
            @endforeach

            </tbody>
        </table>



    </div>
</div>
@endsection