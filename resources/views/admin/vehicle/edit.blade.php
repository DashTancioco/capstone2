@extends('admin.components.layout')

@section('title')
Car Rental Portal | Admin Post Vehicle
@endsection


@section('content')

<h2 class="page-title">Edit Vehicle</h2>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Basic Info</div>
            <div class="panel-body">
                @if(session('status'))
                <div class="succWrap"><strong>SUCCESS</strong>:{{session('msg')}}</div>
                @endif
                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{route('admin.update_vehicle', $result->id)}}">
                	@csrf
                	@method('put')
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vehicle Title<span style="color:red">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" name="vehicletitle" class="form-control"
                                value="<?php echo htmlentities($result->VehiclesTitle)?>" required>
                        </div>
                        <label class="col-sm-2 control-label">Select Brand<span style="color:red">*</span></label>
                        <div class="col-sm-4">
                            <select class="selectpicker" name="brandname" id="brandname" required>
                                @foreach($brands as $brand)
                                <option value="{{$brand->id}}">{{$brand->BrandName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="hr-dashed"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vehical Overview<span style="color:red">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="vehicalorcview" rows="3"
                                required><?php echo htmlentities($result->VehiclesOverview);?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Price Per Day(in USD)<span
                                style="color:red">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" name="priceperday" class="form-control"
                                value="<?php echo htmlentities($result->PricePerDay);?>" required>
                        </div>
                        <label class="col-sm-2 control-label">Select Fuel Type<span style="color:red">*</span></label>
                        <div class="col-sm-4">
                            <select class="selectpicker" name="fueltype" id="fueltype" required>
                                <option value="Petrol">Petrol</option>
                                <option value="Diesel">Diesel</option>
                                <option value="CNG">CNG</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Model Year<span style="color:red">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" name="modelyear" class="form-control"
                                value="<?php echo htmlentities($result->ModelYear);?>" required>
                        </div>
                        <label class="col-sm-2 control-label">Seating Capacity<span style="color:red">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" name="seatingcapacity" class="form-control"
                                value="<?php echo htmlentities($result->SeatingCapacity);?>" required>
                        </div>
                    </div>
                    <div class="hr-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <h4><b>Vehicle Images</b></h4>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-4">
                            Image 1 <img src="{{asset('admin/img/vehicleimages/'.$result->Vimage1)}}" width="300"
                                height="200" style="border:solid 1px #000">

                            <!-- <a href="changeimage1.php?imgid=<?php echo htmlentities($result->id)?>">Change
                                Image 1</a> -->
                            <span style="color:red">*</span><input type="file"
                                name="img1">
                        </div>
                        <div class="col-sm-4">
                            Image 2<img src="{{asset('admin/img/vehicleimages/'.$result->Vimage2)}}" width="300"
                                height="200" style="border:solid 1px #000">
                            <span style="color:red">*</span><input type="file"
                                name="img2">
                            <!-- <a href="changeimage2.php?imgid=<?php echo htmlentities($result->id)?>">Change
                                Image 2</a> -->
                        </div>
                        <div class="col-sm-4">
                            Image 3<img src="{{asset('admin/img/vehicleimages/'.$result->Vimage3)}}" width="300"
                                height="200" style="border:solid 1px #000">
                            <span style="color:red">*</span><input type="file"
                                name="img3">
                      <!--       <a href="changeimage3.php?imgid=<?php echo htmlentities($result->id)?>">Change
                                Image 3</a> -->
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-4">
                            Image 4<img src="{{asset('admin/img/vehicleimages/'.$result->Vimage4)}}" width="300"
                                height="200" style="border:solid 1px #000">
                            <span style="color:red">*</span><input type="file"
                                name="img4">
                           <!--  <a href="changeimage4.php?imgid=<?php echo htmlentities($result->id)?>">Change
                                Image 4</a> -->
                        </div>
                        <div class="col-sm-4">
                            Image 5
                            <?php if($result->Vimage5=="")
							{
							echo htmlentities("File not available");
							} else {?>
                            <img src="{{asset('admin/img/vehicleimages/'.$result->Vimage5)}}" width="300" height="200"
                                style="border:solid 1px #000">
                            <a href="changeimage5.php?imgid=<?php echo htmlentities($result->id)?>">Change
                                Image 5</a>
                            <?php } ?>
                            <span style="color:red">*</span><input type="file"
                                name="img5">
                        </div>

                    </div>
                    <div class="hr-dashed"></div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Accessories</div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-3">
                        <?php if($result->AirConditioner==1)
						{?>
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="airconditioner" checked value="1">
                            <label for="inlineCheckbox1"> Air Conditioner </label>
                        </div>
                        <?php } else { ?>
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="airconditioner" value="1">
                            <label for="inlineCheckbox1"> Air Conditioner </label>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-3">
                        <?php if($result->PowerDoorLocks==1)
						{?>
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="powerdoorlocks" checked value="1">
                            <label for="inlineCheckbox2"> Power Door Locks </label>
                        </div>
                        <?php } else {?>
                        <div class="checkbox checkbox-success checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="powerdoorlocks" value="1">
                            <label for="inlineCheckbox2"> Power Door Locks </label>
                        </div>
                        <?php }?>
                    </div>
                    <div class="col-sm-3">
                        <?php if($result->AntiLockBrakingSystem==1)
						{?>
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="antilockbrakingsys" checked value="1">
                            <label for="inlineCheckbox3"> AntiLock Braking System </label>
                        </div>
                        <?php } else {?>
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="antilockbrakingsys" value="1">
                            <label for="inlineCheckbox3"> AntiLock Braking System </label>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-3">
                        <?php if($result->BrakeAssist==1)
						{
						?>
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="brakeassist" checked value="1">
                            <label for="inlineCheckbox3"> Brake Assist </label>
                        </div>
                        <?php } else {?>
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="brakeassist" value="1">
                            <label for="inlineCheckbox3"> Brake Assist </label>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php if($result->PowerSteering==1)
						{
						?>
                    <div class="col-sm-3">
                        <div class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" name="powersteering" checked value="1">
                            <label for="inlineCheckbox1"> Power Steering </label>
                        </div>
                        <?php } else {?>
                        <div class="col-sm-3">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox1" name="powersteering" value="1">
                                <label for="inlineCheckbox1"> Power Steering </label>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-sm-3">
                            <?php if($result->DriverAirbag==1)
								{
								?>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox1" name="driverairbag" checked value="1">
                                <label for="inlineCheckbox2">Driver Airbag</label>
                            </div>
                            <?php } else { ?>
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox1" name="driverairbag" value="1">
                                <label for="inlineCheckbox2">Driver Airbag</label>
                            </div>
                            <?php } ?>
                        </div>
                            <div class="col-sm-3">
                                <?php if($result->DriverAirbag==1)
									{
									?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="passengerairbag" checked
                                        value="1">
                                    <label for="inlineCheckbox3"> Passenger Airbag </label>
                                </div>
                                <?php } else { ?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="passengerairbag" value="1">
                                    <label for="inlineCheckbox3"> Passenger Airbag </label>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">
                                <?php if($result->PowerWindows==1)
									{
									?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="powerwindow" checked value="1">
                                    <label for="inlineCheckbox3"> Power Windows </label>
                                </div>
                                <?php } else { ?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="powerwindow" value="1">
                                    <label for="inlineCheckbox3"> Power Windows </label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <?php if($result->CDPlayer==1)
										{
										?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="cdplayer" checked value="1">
                                    <label for="inlineCheckbox1"> CD Player </label>
                                </div>
                                <?php } else {?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="cdplayer" value="1">
                                    <label for="inlineCheckbox1"> CD Player </label>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">
                                <?php if($result->CentralLocking==1)
										{
										?>
                                <div class="checkbox  checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="centrallocking" checked value="1">
                                    <label for="inlineCheckbox2">Central Locking</label>
                                </div>
                                <?php } else { ?>
                                <div class="checkbox checkbox-success checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="centrallocking" value="1">
                                    <label for="inlineCheckbox2">Central Locking</label>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">
                                <?php if($result->CrashSensor==1)
										{
										?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="crashcensor" checked value="1">
                                    <label for="inlineCheckbox3"> Crash Sensor </label>
                                </div>
                                <?php } else {?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="crashcensor" value="1">
                                    <label for="inlineCheckbox3"> Crash Sensor </label>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">
                                <?php if($result->CrashSensor==1)
										{
										?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="leatherseats" checked value="1">
                                    <label for="inlineCheckbox3"> Leather Seats </label>
                                </div>
                                <?php } else { ?>
                                <div class="checkbox checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" name="leatherseats" value="1">
                                    <label for="inlineCheckbox3"> Leather Seats </label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">

                                <button class="btn btn-primary" name="submit" type="submit" style="margin-top:4%">Save
                                    changes</button>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        	$(function(){
        		brand_id="<?php echo $result->VehiclesBrand; ?>";
        		fuel="<?php echo $result->FuelType; ?>";
        		console.log("fff", brand_id, fuel);
        		$("#brandname").val(brand_id);
        		$("#fueltype").val(fuel);
        	})
        </script>
        @endsection