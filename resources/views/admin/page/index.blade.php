@extends('admin.components.layout')
@section('title')
Car Rental Portal | Admin Dashboard
@endsection
@section('content')

<script type="text/javascript" src="{{asset('admin/nicEdit.js')}}"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<h2 class="page-title">Manage Pages </h2>

<div class="row">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">Form fields</div>
            <div class="panel-body">
                <form method="post" class="form-horizontal" action="{{route('admin.page_detail.update')}}">
                	@csrf

                    @isset($msg)
                    <div class="succWrap"><strong>SUCCESS</strong>:{{$msg}}</div>
                    @endisset
				
                    <div class="form-group">
                        <label class="col-sm-4 control-label">select Page</label>
                        <div class="col-sm-8">
                            <select name="menu1" id="page_type">
                                <option value="">***Select One***</option>
                                <option value="terms">terms and condition</option>
                                <option value="privacy">privacy and policy</option>
                                <option value="aboutus">aboutus</option>
                                <option value="faqs">FAQs</option>
                            </select>
                        </div>
                    </div>
                    <div class="hr-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">selected Page</label>
                        <div class="col-sm-8" id="page-title">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Page Details </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="5" cols="50" name="pgedetails" id="pgedetails"
                                placeholder="Package Details" required>
                                	
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">

                            <button type="submit" name="submit" value="Update" id="submit"
                                class="btn-primary btn">Update</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

</div>




<script type="text/javascript">
	function get_pageinfo(){
		page_type=$("#page_type").val();
		$.ajax({
			url:"{{route('admin.get_detail')}}",
			type:'post',
			data:{
				_token:"{{csrf_token()}}",
				page_type:page_type,
			},
			success:function(result){
				console.log("result", result);
				$("#page-title").html(result.PageName);
				
				nicEditors.findEditor('pgedetails').setContent(result.detail);


				// $("#pgedetails").val(result.detail);
			},
			error:function(e){
				console.log(e);
			}
		})
	}

	$("#page_type").click(function(){
		get_pageinfo();
	})
</script>
<!-- @isset($type)
<script type="text/javascript">
    type="<?php echo $type; ?>";
    $("#page_type").val(type);
    nicEditors.findEditor('pgedetails').setContent('');
    get_pageinfo();
</script>
@endisset -->


@endsection