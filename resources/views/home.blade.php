@extends('layout.app')
@section('page_title')
    Car Rental Portal
@endsection
@section('content')
<!-- Banners -->
<section id="banner" class="banner-section">
    <div class="container">
        <div class="div_zindex">
            <div class="row">
                <div class="col-md-5 col-md-push-7">
                    <div class="banner_content">
                        <h1>Find the right car for you.</h1>
                        <p>We have more than a thousand cars for you to choose. </p>
                        <a href="#" class="btn">Read More <span class="angle_arrow"><i class="fa fa-angle-right"
                                    aria-hidden="true"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Banners -->


<!-- Resent Cat-->
<section class="section-padding gray-bg">
    <div class="container">
        <div class="section-header text-center">
            <h2>Find the Best <span>CarForYou</span></h2>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration
                in some form, by injected humour, or randomised words which don't look even slightly believable. If you
                are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden
                in the middle of text.</p>
        </div>
        <div class="row">

            <!-- Nav tabs -->
            <div class="recent-tab">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#resentnewcar" role="tab" data-toggle="tab">New
                            Car</a></li>
                </ul>
            </div>
            <!-- Recently Listed New Cars -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="resentnewcar">
                @if(count($results)>0)
                @foreach($results as $key => $result)

                    <div class="col-list-3">
                        <div class="recent-car-list">
                            <div class="car-info-box"> 
                                <a href="{{route('vehical_details',$result->id)}}">
                                    <img src="admin/img/vehicleimages/{{$result->Vimage1}}"
                                        class="img-responsive" alt="image"></a>
                                <ul>
                                    <li><i class="fa fa-car"
                                            aria-hidden="true"></i> {{$result->FuelType}} </li>
                                    <li><i class="fa fa-calendar"
                                            aria-hidden="true"></i> {{$result->ModelYear}}  Model
                                    </li>
                                    <li><i class="fa fa-user"
                                            aria-hidden="true"></i>{{$result->SeatingCapacity}} seats</li>
                                </ul>
                            </div>
                            <div class="car-title-m">
                                <h6>
                                    <a href="{{route('vehical_details',$result->id)}}">
                                        {{$result->brand->BrandName.",".$result->VehiclesTitle}}
                                    </a>
                                    </h6>
                                <span class="price">PHP {{$result->PricePerDay}}/Day</span>
                            </div>
                            <div class="inventory_info_m">
                                <p>  <?php echo substr($result->VehiclesOverview,0,70);?></p>
                            </div>
                        </div>
                    </div>
                @endforeach
                @endif
                 

                </div>
            </div>
        </div>
</section>
<!-- /Resent Cat -->

<!-- Fun Facts-->
<section class="fun-facts-section">
    <div class="container div_zindex">
        <div class="row">
            <div class="col-lg-3 col-xs-6 col-sm-3">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fa fa-calendar" aria-hidden="true"></i>40+</h2>
                        <p>Years In Business</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 col-sm-3">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fa fa-car" aria-hidden="true"></i>1200+</h2>
                        <p>New Cars For Sale</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 col-sm-3">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fa fa-car" aria-hidden="true"></i>1000+</h2>
                        <p>Used Cars For Sale</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 col-sm-3">
                <div class="fun-facts-m">
                    <div class="cell">
                        <h2><i class="fa fa-user-circle-o" aria-hidden="true"></i>600+</h2>
                        <p>Satisfied Customers</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Fun Facts-->


<!--Testimonial -->
<section class="section-padding testimonial-section parallex-bg">
    <div class="container div_zindex">
        <div class="section-header white-text text-center">
            <h2>Our Satisfied <span>Customers</span></h2>
        </div>
        <div class="row">
            <div id="testimonial-slider">
            
                @foreach($testimonials as $key => $testimonial)
                <div class="testimonial-m">
                    <div class="testimonial-content">
                        <div class="testimonial-heading">
                            <h5>{{$testimonial->user->username}}</h5>
                            <p>{{$testimonial->Testimonial}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Testimonial-->

@endsection