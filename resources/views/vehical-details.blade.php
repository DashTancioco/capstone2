@extends('layout.app')
@section('page_title')
Car Rental Portal | Vehicle Details
@endsection
@section('content')

<!--Listing-Image-Slider-->


<section id="listing_img_slider">
    <div><img src="{{asset('admin/img/vehicleimages/'.$result->Vimage1)}}" class="img-responsive"
            alt="image" width="900" height="560"></div>
    <div><img src="{{asset('admin/img/vehicleimages/'.$result->Vimage2)}}" class="img-responsive"
            alt="image" width="900" height="560"></div>
    <div><img src="{{asset('admin/img/vehicleimages/'.$result->Vimage3)}}" class="img-responsive"
            alt="image" width="900" height="560"></div>
    <div><img src="{{asset('admin/img/vehicleimages/'.$result->Vimage4)}}" class="img-responsive"
            alt="image" width="900" height="560"></div>
    @if($result->Vimage5!="")
    <div><img src="{{asset('admin/img/vehicleimages/'.$result->Vimage5)}}" class="img-responsive"
            alt="image" width="900" height="560"></div>
    @endif
</section>
<!--/Listing-Image-Slider-->


<!--Listing-detail-->
<section class="listing-detail">
    <div class="container">
        <div class="listing_detail_head row">
            <div class="col-md-9">
                <h2><?php echo htmlentities($result->brand->BrandName);?> , <?php echo htmlentities($result->VehiclesTitle);?>
                </h2>
            </div>
            <div class="col-md-3">
                <div class="price_info">
                    <p>PHP <?php echo htmlentities($result->PricePerDay);?> </p>Per Day

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="main_features">
                    <ul>

                        <li> <i class="fa fa-calendar" aria-hidden="true"></i>
                            <h5><?php echo htmlentities($result->ModelYear);?></h5>
                            <p>Reg.Year</p>
                        </li>
                        <li> <i class="fa fa-cogs" aria-hidden="true"></i>
                            <h5><?php echo htmlentities($result->FuelType);?></h5>
                            <p>Fuel Type</p>
                        </li>

                        <li> <i class="fa fa-user-plus" aria-hidden="true"></i>
                            <h5><?php echo htmlentities($result->SeatingCapacity);?></h5>
                            <p>Seats</p>
                        </li>
                    </ul>
                </div>
                <div class="listing_more_info">
                    <div class="listing_detail_wrap">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs gray-bg" role="tablist">
                            <li role="presentation" class="active"><a href="#vehicle-overview "
                                    aria-controls="vehicle-overview" role="tab" data-toggle="tab">Vehicle Overview </a>
                            </li>

                            <li role="presentation"><a href="#accessories" aria-controls="accessories" role="tab"
                                    data-toggle="tab">Accessories</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- vehicle-overview -->
                            <div role="tabpanel" class="tab-pane active" id="vehicle-overview">

                                <p><?php echo htmlentities($result->VehiclesOverview);?></p>
                            </div>


                            <!-- Accessories -->
                            <div role="tabpanel" class="tab-pane" id="accessories">
                                <!--Accessories-->
                                <table>
                                    <thead>
                                        <tr>
                                            <th colspan="2">Accessories</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Air Conditioner</td>
                                            @if($result->AirConditioner==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>AntiLock Braking System</td>
                                            @if($result->AntiLockBrakingSystem==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Power Steering</td>
                                            @if($result->PowerSteering==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>


                                        <tr>

                                            <td>Power Windows</td>

                                            @if($result->PowerWindows==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>CD Player</td>
                                            @if($result->CDPlayer==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Leather Seats</td>
                                            @if($result->LeatherSeats==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Central Locking</td>
                                            @if($result->CentralLocking==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Power Door Locks</td>
                                            @if($result->PowerDoorLocks==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>Brake Assist</td>
                                            @if($result->BrakeAssist==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Driver Airbag</td>
                                            @if($result->DriverAirbag==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Passenger Airbag</td>
                                            @if($result->PassengerAirbag==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                        <tr>
                                            <td>Crash Sensor</td>
                                            @if($result->CrashSensor==1)
                                            <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                            @else
                                            <td><i class="fa fa-close" aria-hidden="true"></i></td>
                                            @endif
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!--Side-Bar-->
            <aside class="col-md-3">

                <div class="share_vehicle">
                    <p>Share: <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a> <a href="#"><i
                                class="fa fa-twitter-square" aria-hidden="true"></i></a> <a href="#"><i
                                class="fa fa-linkedin-square" aria-hidden="true"></i></a> <a href="#"><i
                                class="fa fa-google-plus-square" aria-hidden="true"></i></a> </p>
                </div>
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fa fa-envelope" aria-hidden="true"></i>Book Now</h5>
                    </div>
                    <form method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="fromdate" id="fromdate" placeholder="From Date(dd/mm/yyyy)"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="todate" id="todate" placeholder="To Date(dd/mm/yyyy)"
                                required>
                        </div>
                        <div class="form-group">
                            <textarea rows="4" class="form-control" name="message" placeholder="Message" id="message"
                                required></textarea>
                        </div>

                        @auth
                        <div class="form-group">
                            <input type="button" class="btn" name="submit" value="Book Now" id="book-submit">
                        </div>
                        @else
                        <a href="#loginform" class="btn btn-xs uppercase" data-toggle="modal" data-dismiss="modal">Login For Book</a>
                        @endauth
                    </form>
                </div>
            </aside>
            <!--/Side-Bar-->
        </div>

        <div class="space-20"></div>
        <div class="divider"></div>

        <!--Similar-Cars-->
        <div class="similar_cars">
            <h3>Similar Cars</h3>
            <div class="row">
				@if(count($brands) > 0)
				@foreach($brands as $result)
                <div class="col-md-3 grid_listing">
                    <div class="product-listing-m gray-bg">
                        <div class="product-listing-img"> 
                        	<a href="{{route('vehical_details',$result->id)}}">
                        		<img src="{{asset('admin/img/vehicleimages/'.$result->Vimage1)}}"
                                    class="img-responsive" alt="image" /> </a>
                        </div>
                        <div class="product-listing-content">
                            <h5><a href="{{route('vehical_details',$result->id)}}">
                            	<?php echo htmlentities($result->BrandName);?>
                                    , <?php echo htmlentities($result->VehiclesTitle);?></a></h5>
                            <p class="list-price">PHP <?php echo htmlentities($result->PricePerDay);?></p>

                            <ul class="features_list">

                                <li><i class="fa fa-user"
                                        aria-hidden="true"></i><?php echo htmlentities($result->SeatingCapacity);?>
                                    seats</li>
                                <li><i class="fa fa-calendar"
                                        aria-hidden="true"></i><?php echo htmlentities($result->ModelYear);?> model</li>
                                <li><i class="fa fa-car"
                                        aria-hidden="true"></i><?php echo htmlentities($result->FuelType);?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif

            </div>
        </div>
        <!--/Similar-Cars-->

    </div>
</section>
<!--/Listing-detail-->


<script type="text/javascript">
	$("#book-submit").click(function(){
        VehicleId="<?php echo $result->id; ?>"
		fromdate=$("#fromdate").val();
		todate=$("#todate").val();
		message=$("#message").val();
		console.log("asdf",message)
		$.ajax({
			url:"{{route('insert_book')}}",
			data:{
				_token: "{{csrf_token()}}",
				FromDate:fromdate,
				ToDate:todate,
				message:message,
                VehicleId:VehicleId,
			},
			method:"post",
			success:function(result){
				console.log(result,"result");
                alert('Booking successfull.');
                $("#fromdate").val('');
                $("#todate").val('');
                $("#message").val('');
			},
			error:function (e){
				console.log(e);
			}
		})

	})

</script>


@endsection