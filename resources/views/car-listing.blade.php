@extends('layout.app')
@section('page_title')
Car Rental Portal | Car Listing
@endsection
@section('content')


<!--Page Header-->
<section class="page-header listing_page">
    <div class="container">
        <div class="page-header_wrap">
            <div class="page-heading">
                <h1>Car Listing</h1>
            </div>
            <ul class="coustom-breadcrumb">
                <li><a href="#">Home</a></li>
                <li>Car Listing</li>
            </ul>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Page Header-->

<!--Listing-->
<section class="listing-page">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <div class="result-sorting-wrapper">
                    <div class="sorting-count">
                        <p><span>{{count($vehicles)}} Listings</span></p>
                    </div>
                </div>
                @if(count($vehicles) >0)
                @foreach($vehicles as $result)
                <div class="product-listing-m gray-bg">
                    <div class="product-listing-img">
                        <img src="admin/img/vehicleimages/{{$result->Vimage1}}"
                            class="img-responsive" alt="Image" /> </a>
                    </div>
                    <div class="product-listing-content">
                        <h5>
                        	<a href="{{route('vehical_details',$result->id)}}">
                        		{{$result->brand->BrandName}} , {{$result->VehiclesTitle}}
                            </a>
                        </h5>
                        <p class="list-price">PHP {{$result->PricePerDay}} Per Day</p>
                        <ul>
                            <li><i class="fa fa-user"
                                    aria-hidden="true"></i>{{$result->SeatingCapacity}} seats
                            </li>
                            <li><i class="fa fa-calendar"
                                    aria-hidden="true"></i>{{$result->ModelYear}} model</li>
                            <li><i class="fa fa-car"
                                    aria-hidden="true"></i>{{$result->FuelType}}</li>
                        </ul>
                        <a href="{{route('vehical_details',$result->id)}}" class="btn">View
                            Details <span class="angle_arrow"><i class="fa fa-angle-right"
                                    aria-hidden="true"></i></span></a>
                    </div>
                </div>
                @endforeach
                @endif
            </div>

            <!--Side-Bar-->
            <aside class="col-md-3 col-md-pull-9">
                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fa fa-filter" aria-hidden="true"></i> Find Your Car </h5>
                    </div>
                    <div class="sidebar_filter">
                        <form action="{{route('search_carresult')}}" method="post">
                        	@csrf
                            <div class="form-group select">
                                <select class="form-control" name="brand" id="brand">
                                    <option value="">Select Brand</option>
                                    @if(count($brands)>0)
                                    @foreach($brands as $key => $brand)
                                    <option value="{{$brand->id}}">{{$brand->BrandName}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group select">
                                <select class="form-control" name="fueltype" id="fueltype">
                                    <option value="">Select Fuel Type</option>
                                    <option value="Petrol">Petrol</option>
                                    <option value="Diesel">Diesel</option>
                                    <option value="CNG">CNG</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-block"><i class="fa fa-search"
                                        aria-hidden="true"></i> Search Car</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="sidebar_widget">
                    <div class="widget_heading">
                        <h5><i class="fa fa-car" aria-hidden="true"></i> Recently Listed Cars</h5>
                    </div>
                    <div class="recent_addedcars">
                        <ul>

                            @if(count($last_vehicles)>0)
                            @foreach($last_vehicles as $key => $result)
                            <li class="gray-bg">
                                <div class="recent_post_img">
                                    <a href="{{route('vehical_details',$result->id)}}">
                                    	<img
                                            src="admin/img/vehicleimages/{{$result->Vimage1}}"
                                            alt="image">
                                    </a> 

                                </div>
                                <div class="recent_post_title"> 
                                	<a href="{{route('vehical_details',$result->id)}}">{{$result->brand->BrandName}}
                                        , {{$result->VehiclesTitle}}</a>
                                    <p class="widget_price">PHP {{$result->PricePerDay}} Per Day
                                    </p>
                                </div>
                            </li>
                            @endforeach
                            @endif

                        </ul>

                    </div>
                </div>
            </aside>
            <!--/Side-Bar-->
        </div>
    </div>
</section>
<!-- /Listing-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@isset($search)
<script type="text/javascript">
	$(function(){
		brand_id="<?php echo $brand_id ?>";
		fueltype_id="<?php echo $fueltype_id ?>";
		$("#brand").val(brand_id);
		$("#fueltype").val(fueltype_id);
		console.log(brand_id, fueltype_id)

	})
</script>
@endisset



@endsection