<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('VehiclesTitle')->nulllable();
            $table->integer('VehiclesBrand')->nulllable();
            $table->text('VehiclesOverview')->nulllable();
            $table->integer('PricePerDay')->nulllable();
            $table->string('FuelType')->nulllable();
            $table->integer('ModelYear')->nulllable();
            $table->integer('SeatingCapacity')->nulllable();
            $table->string('Vimage1')->nulllable();
            $table->string('Vimage2')->nulllable();
            $table->string('Vimage3')->nulllable();
            $table->string('Vimage4')->nulllable();
            $table->string('Vimage5')->nulllable();
            $table->integer('AirConditioner')->nulllable();
            $table->integer('PowerDoorLocks')->nulllable();
            $table->integer('AntiLockBrakingSystem')->nulllable();
            $table->integer('BrakeAssist')->nulllable();
            $table->integer('PowerSteering')->nulllable();
            $table->integer('DriverAirbag')->nulllable();
            $table->integer('PassengerAirbag')->nulllable();
            $table->integer('PowerWindows')->nulllable();
            $table->integer('CDPlayer')->nulllable();
            $table->integer('CentralLocking')->nulllable();
            $table->integer('CrashSensor')->nulllable();
            $table->integer('LeatherSeats')->nulllable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
