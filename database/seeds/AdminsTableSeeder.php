<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'id' => 1,
                'UserName' => 'admin',
                'Password' => '$2y$10$TeMQMm0w0lHMc1F4k0cKCOOdxsursrYxy3zcwczOyzXBeOeNshFzO',
                'created_at' => '2020-04-23 06:23:19',
                'updated_at' => '2020-04-23 06:23:19',
            ),
        ));
        
        
    }
}