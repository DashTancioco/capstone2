<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function vehicle(){
    	return $this->belongsTo('App\Vehicle','VehicleId','id');
    }

    public function user(){
    	return $this->belongsTo('App\User','user_id','id');
    }

}
