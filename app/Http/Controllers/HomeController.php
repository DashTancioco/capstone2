<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Vehicle;
use App\Testimonial;
use App\Brand;
use App\Booking;
use App\Contactinfo;
use App\Contactquery;
use App\Subscriber;

class HomeController extends Controller
{
    public function home(){
        $results=Vehicle::all();
        $testimonials=Testimonial::where('status',1)->get();
        return view('home', compact('results', 'testimonials'));
    }

    public function index(){
        $is_admin=auth()->user()->is_admin;
        if($is_admin==1){
            return redirect()->route('admin.change_pass');
        }else{
            return redirect()->route('home');
        }
    }

    public function car_lising(){
        $vehicles=Vehicle::all();
        $brands=Brand::all();
        $last_vehicles=Vehicle::orderBy('id','desc')->limit(4)->get();
        return view('car-listing',compact('vehicles','brands','last_vehicles'));
    }

    public function search_carresult(Request $request){
        $brand_id=$request->brand;
        $fueltype_id=$request->fueltype;
        $query=Vehicle::orderBy('id','desc');
        if($brand_id != ''){
            $query->where('VehiclesBrand',$brand_id);
        }

        if($fueltype_id != ''){
            $query->where('FuelType',$fueltype_id);
        }
        $vehicles=$query->get();
        $brands=Brand::all();
        $last_vehicles=Vehicle::orderBy('id','desc')->limit(4)->get();
        $search=1;
        return view('car-listing',compact('vehicles','brands','last_vehicles','brand_id','fueltype_id','search'));
    }

    public function vehical_details($vhid){
        $result=Vehicle::find($vhid);
        $brands=Vehicle::where('VehiclesBrand',$result->VehiclesBrand)->get();
    	return view('vehical-details',compact('result','brands'));
    }

    public function insert_book(Request $request){
        $new_booking=new Booking();
        $new_booking->user_id=Auth::id();
        $new_booking->FromDate=$request->FromDate;
        $new_booking->ToDate=$request->ToDate;
        $new_booking->message=$request->message;
        $new_booking->status=0;
        $new_booking->VehicleId=$request->VehicleId;
        $new_booking->save();
        return $new_booking;
    }

    public function contact_us(){
        $contactinfo=Contactinfo::all();
        return view('contact-us', compact('contactinfo'));
    }

    public function send_post(Request $request){
        $query=new Contactquery();
        $query->name=$request->name;
        $query->EmailId=$request->EmailId;
        $query->ContactNumber=$request->ContactNumber;
        $query->Message=$request->Message;
        $query->save();
        return redirect()->route('contact_us')->with('success','Query Sent. We will contact you shortly');
    }    

    public function email_subscibe(Request $request){
        $email=$request->email;
        $count=Subscriber::where('SubscriberEmail',$email)->count();
        if($count>0){
            return 0;
        }
        $subscriber=new Subscriber();
        $subscriber->SubscriberEmail=$email;
        $subscriber->save();
        return 1;
    }
}
