<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;


use Illuminate\Http\Request;
use App\User;
use App\Booking;
use App\Testimonial;

class UserController extends Controller
{
    public function profile(){

    	return view('user.profile');
    }
    public function profile_update(Request $request, $user_id){
    	$user=User::find($user_id);
    	$user->update($request->except('_token',"_method"));
    	return redirect()->back()->with('success','Profile Updated Successfully');
    }

    public function pass_update(Request $request){
    	$user=User::find(auth()->user()->id);
    	$old_pass=$request->password;
    	$new_pass=$request->newpassword;

    	if(Hash::check($old_pass, $user->password) == false){
           return redirect()->back()->with(['status'=>'error','msg'=>'Your current password is wrong']);
        }
        $user->password = Hash::make($new_pass);
        $user->save();
        return redirect()->back()->with(['status'=>'success','msg'=>'Your Password succesfully changed']);
    }
    public function my_booking(){
    	$bookings=Booking::where('user_id',auth()->user()->id)->get();
    	return view('user.my_booking', compact('bookings'));
    }

    public function post_test(Request $request){
    	$testimonial=new Testimonial();
    	$testimonial->Testimonial=$request->testimonial;
    	$testimonial->user_id=auth()->user()->id;
    	$testimonial->save();
        return redirect()->back()->with(['status'=>'success','msg'=>'Testimonail submitted successfully']);
    }

    public function my_testimonials(){
    	$testimonials=Testimonial::where('user_id',auth()->user()->id)->get();
    	return view('user.my_test', compact('testimonials'));
    }

}
