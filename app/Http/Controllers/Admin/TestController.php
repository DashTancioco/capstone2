<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Testimonial;

class TestController extends Controller
{
    public function index(){
    	$tests=Testimonial::all();

    	return view('admin.test', compact('tests'));
    }

    public function test_change($id, $status){
    	$test=Testimonial::find($id);
    	$test->status=$status;
    	$test->save();
    	$msg=$status==0?'Testimonial Successfully Inactive':'Testimonial Successfully Active';
    	return redirect()->back()->with(['status'=>'success', 'msg'=>$msg]);
    }
}
