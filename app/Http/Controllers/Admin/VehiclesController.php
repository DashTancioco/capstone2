<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Vehicle;
use App\Brand;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles=Vehicle::all();
        return view('admin.vehicle.list', compact('vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehicle=new Vehicle();

        $request->img1->move(public_path('admin/img/vehicleimages'), time().$_FILES["img1"]["name"]);
        $request->img2->move(public_path('admin/img/vehicleimages'), time().$_FILES["img2"]["name"]);
        $request->img3->move(public_path('admin/img/vehicleimages'), time().$_FILES["img3"]["name"]);
        $request->img4->move(public_path('admin/img/vehicleimages'), time().$_FILES["img4"]["name"]);
        if($request->img5){
            $request->img5->move(public_path('admin/img/vehicleimages'), time().$_FILES["img5"]["name"]);
        }
        $vehicle->VehiclesTitle=$request->vehicletitle;
        $vehicle->VehiclesBrand=$request->brandname;
        $vehicle->VehiclesOverview=$request->vehicalorcview; 
        $vehicle->PricePerDay=$request->priceperday;
        $vehicle->FuelType=$request->fueltype;
        $vehicle->ModelYear=$request->modelyear;
        $vehicle->SeatingCapacity=$request->seatingcapacity;
        $vehicle->Vimage1=time().$_FILES["img1"]["name"];
        $vehicle->Vimage2=time().$_FILES["img2"]["name"];
        $vehicle->Vimage3=time().$_FILES["img3"]["name"];
        $vehicle->Vimage4=time().$_FILES["img4"]["name"];
        if($request->img5){
            $vehicle->Vimage5=time().$_FILES["img5"]["name"];
        }
        $vehicle->AirConditioner=$request->airconditioner;
        $vehicle->PowerDoorLocks=$request->powerdoorlocks;
        $vehicle->AntiLockBrakingSystem=$request->antilockbrakingsys;
        $vehicle->BrakeAssist=$request->brakeassist;
        $vehicle->PowerSteering=$request->powersteering;
        $vehicle->DriverAirbag=$request->driverairbag;
        $vehicle->PassengerAirbag=$request->passengerairbag;
        $vehicle->PowerWindows=$request->powerwindow;
        $vehicle->CDPlayer=$request->cdplayer;
        $vehicle->CentralLocking=$request->centrallocking;
        $vehicle->CrashSensor=$request->crashcensor;
        $vehicle->LeatherSeats=$request->leatherseats;
        $vehicle->save();
        return redirect()->back()->with(['status'=>'success','msg'=>'Vehicle posted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result=Vehicle::find($id);
        $brands=Brand::all();

        return view('admin.vehicle.edit',compact('result','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehicle=Vehicle::find($id);
        if($request->img1){
            $request->img1->move(public_path('admin/img/vehicleimages'), time().$_FILES["img1"]["name"]);
            $vehicle->Vimage1=time().$_FILES["img1"]["name"];
        }
        if($request->img2){
            $request->img2->move(public_path('admin/img/vehicleimages'), time().$_FILES["img2"]["name"]);
            $vehicle->Vimage2=time().$_FILES["img2"]["name"];
        }
        if($request->img3){
            $request->img3->move(public_path('admin/img/vehicleimages'), time().$_FILES["img3"]["name"]);
            $vehicle->Vimage3=time().$_FILES["img3"]["name"];
        }
        if($request->img4){
            $request->img4->move(public_path('admin/img/vehicleimages'), time().$_FILES["img4"]["name"]);
            $vehicle->Vimage4=time().$_FILES["img4"]["name"];
        }
        if($request->img5){
            $request->img5->move(public_path('admin/img/vehicleimages'), time().$_FILES["img5"]["name"]);
            $vehicle->Vimage5=time().$_FILES["img5"]["name"];
        }

        $vehicle->VehiclesTitle=$request->vehicletitle;
        $vehicle->VehiclesBrand=$request->brandname;
        $vehicle->VehiclesOverview=$request->vehicalorcview; 
        $vehicle->PricePerDay=$request->priceperday;
        $vehicle->FuelType=$request->fueltype;
        $vehicle->ModelYear=$request->modelyear;
        $vehicle->SeatingCapacity=$request->seatingcapacity;

        $vehicle->AirConditioner=$request->airconditioner;
        $vehicle->PowerDoorLocks=$request->powerdoorlocks;
        $vehicle->AntiLockBrakingSystem=$request->antilockbrakingsys;
        $vehicle->BrakeAssist=$request->brakeassist;
        $vehicle->PowerSteering=$request->powersteering;
        $vehicle->DriverAirbag=$request->driverairbag;
        $vehicle->PassengerAirbag=$request->passengerairbag;
        $vehicle->PowerWindows=$request->powerwindow;
        $vehicle->CDPlayer=$request->cdplayer;
        $vehicle->CentralLocking=$request->centrallocking;
        $vehicle->CrashSensor=$request->crashcensor;
        $vehicle->LeatherSeats=$request->leatherseats;
        $vehicle->save();
        return redirect()->back()->with(['status'=>'success','msg'=>'Data updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vehicle::destroy($id);
        return redirect()->back()->with(['status'=>'success','msg'=>'Vehicle  record deleted successfully']);
    }

    public function post(){
        $brands=Brand::all();

        return view('admin.vehicle.post', compact('brands'));
    }
}
