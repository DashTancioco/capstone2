<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Contactinfo;

class InfoController extends Controller
{
    public function index(){
    	$result=Contactinfo::first();
    	return view('admin.contact-info', compact('result'));
    }
    public function store(Request $request, $id){

    	$contact_info=Contactinfo::find($id);
    	$contact_info->Address=$request->address;
    	$contact_info->EmailId=$request->email;
    	$contact_info->ContactNo=$request->contactno;
    	$contact_info->save();
    	return redirect()->back()->with(['status'=>'success', 'msg'=>'Info Updateed successfully']);
    }
}
