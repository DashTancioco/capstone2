<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Contactquery;

class ContactqueryController extends Controller
{
    public function index(){
    	$queries=Contactquery::all();
    	return view('admin.contact-query', compact('queries'));
    }

    public function status_read($id){
    	$query=Contactquery::find($id);
    	$query->status=1;
    	$query->save();
    	return redirect()->back()->with(['status'=>'success', 'msg'=>'Contact Query Read Successfully']);
    }
}
