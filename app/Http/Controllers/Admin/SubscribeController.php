<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Subscriber;

class SubscribeController extends Controller
{
	public function index(){
		$scribers=Subscriber::all();
    	return view('admin.subscriber', compact('scribers'));
	}

	public function delete($id){
		Subscriber::destroy($id);
		return redirect()->back()->with(['status'=>'success', 'msg'=>'Subscriber info deleted']);
	}
    
}
