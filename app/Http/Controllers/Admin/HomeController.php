<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Vehicle;
use App\Testimonial;
use App\Brand;
use App\Booking;
use App\Contactinfo;
use App\Contactquery;
use App\Subscriber;

class HomeController extends Controller
{
    public function index(){
    	return view('admin.login');
    }

    public function update_pass(Request $request){
    	$user=User::find(auth()->user()->id);
    	$old_pass=$request->password;
    	$new_pass=$request->newpassword;

    	if(Hash::check($old_pass, $user->password) == false){
           return redirect()->back()->with(['status'=>'error','msg'=>'Your current password is wrong']);
        }
        $user->password = Hash::make($new_pass);
        $user->save();
        return redirect()->back()->with(['status'=>'success','msg'=>'Your Password succesfully changed']);
    }

    public function dashboard(){
    	$user_count=User::where('is_admin',0)->count();
    	$vehicle_count=Vehicle::count();
    	$booking_count=Booking::count();
    	$brand_count=Brand::count();
    	$scribe_count=Subscriber::count();
    	$query_count=Contactquery::count();
    	$test_count=Testimonial::count();
    	return view('admin.dashboard', compact('user_count','vehicle_count','booking_count','brand_count','scribe_count','query_count','test_count'));
    }

    public function add_brand(Request $request){
        $brand=new Brand();
        $brand->BrandName=$request->brand;
        $brand->save();
        return redirect()->back()->with(['status'=>'success','msg'=>'Brand Created successfully']);
    }

    public function manage_brands(){
        $brands=Brand::all();
        return view('admin.brand.list', compact('brands'));
    }

    public function destory_brands($del_id){
         Brand::destroy($del_id);
        return redirect()->back()->with(['status'=>'success','msg'=>'Brand Deleted successfully']);
    }
    public function edit_brands($id){
        $brand=Brand::find($id);
        return view('admin.brand.edit', compact('brand'));
    }

    public function update_brand(Request $request, $id){
        $brand=Brand::find($id);
        $brand->BrandName=$request->brand;
        $brand->save();
        return redirect()->route('admin.manage_brands')->with(['status'=>'success','msg'=>'Brand Updated successfully']);
    }

    public function manage_bookings(){
        $bookings=Booking::all();
        return view('admin.booking', compact('bookings'));
    }

    public function manage_bookings_confirm($book_id){
        $booking=Booking::find($book_id);
        $booking->Status=1;
        $booking->save();
        return redirect()->route('admin.manage_bookings')->with(['status'=>'success','msg'=>'Booking Successfully Confirmed']);
    }

    public function manage_bookings_cancel($book_id){
        $booking=Booking::find($book_id);
        $booking->Status=2;
        $booking->save();
        return redirect()->route('admin.manage_bookings')->with(['status'=>'success','msg'=>'Booking Successfully Cancelled']);
    }

    public function manage_bookings_complete($book_id){
        $booking=Booking::find($book_id);
        $booking->Status=3;
        $booking->save();
        return redirect()->route('admin.manage_bookings')->with(['status'=>'success','msg'=>'Booking Successfully Completed']);
    }

    public function reg_users(){
        $reg_users=User::where('is_admin','0')->get();
        return view('admin.reg_user', compact('reg_users'));
    }
}
