<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@home')->name('home');

Route::get('home', 'HomeController@index')->name('index');
Route::get('page/{type}', 'PageController@show_page')->name('page');
Route::get('car-listing', 'HomeController@car_lising')->name('car_listing');
Route::post('search-carresult', 'HomeController@search_carresult')->name('search_carresult');
Route::get('vehical-details/{vhid}', 'HomeController@vehical_details')->name('vehical_details');
Route::post('insert_book', 'HomeController@insert_book')->name('insert_book');
Route::get('contact-us', 'HomeController@contact_us')->name('contact_us');
Route::post('send_post', 'HomeController@send_post')->name('send_post');
Route::post('email-subscibe', 'HomeController@email_subscibe')->name('emailsubscibe');

//customer
Route::group(['middleware' => ['web','auth']], function(){
	Route::get('profile', 'UserController@profile')->name('profile');
	Route::put('profile-update/{user_id}', 'UserController@profile_update')->name('profile_update');
	Route::get('update-password', function(){
		return view('user.update_pass');
	})->name('update_password');
	Route::post('pass-update', 'UserController@pass_update')->name('pass_update');
	Route::get('my-booking', 'UserController@my_booking')->name('my_booking');

	Route::get('post-testimonial',  function(){
		return view('user.post_test');
	})->name('post_testimonial');
	Route::post('post-test', 'UserController@post_test')->name('post_test');
	Route::get('my-testimonials', 'UserController@my_testimonials')->name('my_testimonials');
});

//admin
Route::get('admin/login', 'Admin\HomeController@index')->name('admin');

Route::group(['middleware' => ['web','admin']], function(){
	Route::get('admin/change_pass', function(){
		return view('admin.change-pass');
	})->name('admin.change_pass');

	Route::post('admin/update_pass', 'Admin\HomeController@update_pass')->name('admin.update_pass');
	Route::get('admin/dashboard', 'Admin\HomeController@dashboard')->name('admin.dashboard');
	Route::get('admin/create_brand', function(){
		return view('admin.brand.create');
	})->name('admin.create_brand');

	Route::post('admin/add_brand', 'Admin\HomeController@add_brand')->name('admin.add_brand');
	Route::get('admin/manage_brands', 'Admin\HomeController@manage_brands')->name('admin.manage_brands');
	Route::get('admin/edit_brands/{id}', 'Admin\HomeController@edit_brands')->name('admin.brand.edit');
	Route::put('admin/update_brand/{id}', 'Admin\HomeController@update_brand')->name('admin.update_brand');
	Route::get('admin/destory_brands/{del_id}', 'Admin\HomeController@destory_brands')->name('admin.brand.destroy');

	// vehicle
	Route::get('admin/manage_vehicles', 'Admin\VehiclesController@index')->name('admin.manage_vehicles');
	Route::get('admin/post_avehical', 'Admin\VehiclesController@post')->name('admin.post_vehical');
	Route::post('admin/save_vehicle', 'Admin\VehiclesController@store')->name('admin.save_vehicle');
	Route::put('admin/update_vehicle/{id}', 'Admin\VehiclesController@update')->name('admin.update_vehicle');
	Route::get('admin/vehicle_delele/{id}', 'Admin\VehiclesController@destroy')->name('admin.vehicle_delele');
	Route::get('admin/vehicle_edit/{id}', 'Admin\VehiclesController@edit')->name('admin.vehicle_edit');

	// booking
	Route::get('admin/manage_bookings', 'Admin\HomeController@manage_bookings')->name('admin.manage_bookings');
	Route::get('admin/manage_bookings_confirm/{book_id}', 'Admin\HomeController@manage_bookings_confirm')->name('manage-bookings-confirm');
	Route::get('admin/manage_bookings_cancel/{book_id}', 'Admin\HomeController@manage_bookings_cancel')->name('manage-bookings-cancel');
	Route::get('admin/manage_bookings_complete/{book_id}', 'Admin\HomeController@manage_bookings_complete')->name('manage-bookings-complete');

	// testimonials
	Route::get('admin/testimonials', 'Admin\TestController@index')->name('admin.testimonials');
	Route::get('admin/test_change/{id}/{status}', 'Admin\TestController@test_change')->name('admin.test_change');

	// contact query
	Route::get('admin/manage_conactusquery', 'Admin\ContactqueryController@index')->name('admin.manage_conactusquery');
	Route::get('admin/query_read/{id}', 'Admin\ContactqueryController@status_read')->name('admin.status_read');

	//reg users
	Route::get('admin/reg_users', 'Admin\HomeController@reg_users')->name('admin.reg_users');

	// page detail
	Route::get('admin/manage_pages', 'Admin\PageDetailController@index')->name('admin.manage_pages');
	Route::post('admin/get_detail', 'Admin\PageDetailController@get_detail')->name('admin.get_detail');
	Route::post('admin/store', 'Admin\PageDetailController@store')->name('admin.page_detail.update');

	//contact info
	Route::get('admin/update_contactinfo', 'Admin\InfoController@index')->name('admin.update_contactinfo');
	Route::put('admin/store_contact_info/{id}', 'Admin\InfoController@store')->name('admin.store_contact_info');
	
	//subscriber
	Route::get('admin/manage_subscribers', 'Admin\SubscribeController@index')->name('admin.manage_subscribers');
	Route::get('admin/delete_sub/{id}', 'Admin\SubscribeController@delete')->name('admin.delete_sub');

});





